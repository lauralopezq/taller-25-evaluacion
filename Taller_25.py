import pandas as pd
import numpy as np
from statistics import mean

archivo=pd.read_csv("CSV.csv", delimiter=",", encoding="ISO-8859-1")
print(archivo)

print("PUNTO A: Calcule el valor promedio por metro cuadrado de un municipio dado, es decir, el nombre del municipio es un parámetro de la función.")
def Valor_promedio_mun(mun:str):

  filtered_data = archivo[archivo.Municipio == mun]

  return sum(filtered_data.Precio) / sum(filtered_data.Área)

print("PUNTO B: Retorne una lista ordenada de precios para un municipio y tipo dado, es decir, el municipio y el tipo son parámetros de la función")

def lista_de_predios(mun:str, tipo:str, ret:int = 1): 
      params=[('Municipio',mun),('Tipo',tipo)]
selected = archivo
for key, val in params:
    selected = selected[selected[key] == val]
if ret == 1:
 return list(selected.Precio)
elif ret == 2:
      return selected

print ("PUNTO C: Retorne una lista ordenada de los precios que sean iguales o mayores a un valor dado para un municipio y tipo específico. Tanto el valor, como el nombre de municipio y tipo son parámetros de la función.")

def lista_de_predios_umbral(mun:str, tipo:str, precio:int):
      selected = lista_de_predios(mun, tipo, 2)
      selected = selected[selected['Precio']>= precio]
      return list(selected.Precio)
    
print("PUNTO D. Valor promedio del área de todos los predios")
print (archivo['Area'].mean())

print ("PUNTO E: Retorne una lista ordenada de los municipios que tienen predios registrados en la tabla. Los nombres no pueden ir repetidos")

def lista_municipios():
  return list(archivo.Municipio.unique())
 
print ("PUNTO F:Retorne una tupla con las áreas de los predios de un tipo dado. El tipo es un parámetro de la función.")
def lista_areas(tipo:str):
      return list(archivo[archivo.Tipo == tipo].Área.unique())

print ("PUNTO G: Retorne un diccionario con los datos completos de un predio dado. El código del predio es un parámetro de la función")
def ver_predio(cod:int):
      selected = archivo[archivo.Código_predio == cod]
      dictionary = dict()
      for i in range(0,len(archivo.columns)):
            dictionary.setdefault(selected.columns[i],selected.values.tolist()[0][i])
            return dictionary


