import pandas as pd
import numpy as np


archivo=pd.read_csv("CSV.csv", delimiter=",", encoding="ISO-8859-1")
print(archivo)

print("PUNTO A: Calcule el valor promedio por metro cuadrado de un municipio dado, es decir, el nombre del municipio es un parámetro de la función.")
def valor_promedio_mun(mun:str):
      
  filtered_data = archivo[archivo.Municipio == mun]
  
  return sum(filtered_data.precio) / sum(filtered_data.area)

print(valor_promedio_mun)


print ("PUNTO C: Retorne una lista ordenada de los precios que sean iguales o mayores a un valor dado para un municipio y tipo específico. Tanto el valor, como el nombre de municipio y tipo son parámetros de la función.")

def lista_de_predios_umbral(mun:str, tipo:str, precio:int):
      selected = lista_de_predios_umbral(mun, tipo, 2)
      selected = selected[selected['Precio']>= precio]
      return list(selected.Precio)
print(lista_de_predios_umbral)
    
print("PUNTO D. Valor promedio del área de todos los predios")
print (archivo['Area'].mean())

print ("PUNTO E: Retorne una lista ordenada de los municipios que tienen predios registrados en la tabla. Los nombres no pueden ir repetidos")

def lista_municipios():
  return list(archivo.Municipio.unique())
print (lista_municipios)
 
print ("PUNTO F:Retorne una tupla con las áreas de los predios de un tipo dado. El tipo es un parámetro de la función.")
def lista_areas(tipo:str):
      return list(archivo[archivo.Tipo == tipo].Área.unique())
print (lista_areas)

print ("PUNTO G: Retorne un diccionario con los datos completos de un predio dado. El código del predio es un parámetro de la función")
def ver_predio(cod:int):
      selected = archivo[archivo.Código_predio == cod]
      dictionary = dict()
      for i in range(0,len(archivo.columns)):
            dictionary.setdefault(selected.columns[i],selected.values.tolist()[0][i])
            return dictionary




